import "./home.css";
import Trending from "../Trending/trending";
import PopularMovies from "../PopularMovies/latesttrailers";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {actions} from '../../store/globalstate';

const formattedMovieData = (data) => ({
  adult: data.adult,
  backdropPath: data.backdrop_path,
  genreIds: data.genre_ids,
  id: data.id,
  originalLanguage: data.original_language,
  originalTitle: data.original_title,
  overview: data.overview,
  popularity: data.popularity,
  posterPath: data.poster_path,
  releaseDate: data.release_date,
  title: data.title,
  video: data.video,
  voteAverage: data.vote_average,
  voteCount: data.vote_count
})

const Home = () => {
  const searchInput = useSelector((state) => state.searchInput);
  const isClickedOnSearch = useSelector(state=>state.isClickedOnSearch)

  const dispatch = useDispatch();
  const navigate = useNavigate()

  const handleSearch = (event) => {
    // dispatch({type: "SEARCH_INPUT" ,payload: event.target.value});
    dispatch(actions.searchInput(event.target.value))
  };

  const handleSearchButton = () => {
    // dispatch({type:"SEARCH"})
    dispatch(actions.search())
  }

  useEffect(()=>{
    const fetchingSearchedMovie = async () => {
      const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=71025ca4064d6a5209a02f09d64b0c76&language=en-US&query=${searchInput}&page=1&include_adult=false`)
      if (response.ok) {
        const data = await response.json()
        const searchResults = data.results.map((movie)=>formattedMovieData(movie))
        // dispatch({type:"SEARCH_RESULTS", payload: searchResults})
        dispatch(actions.searchResults(searchResults))
      }
    }

    if (isClickedOnSearch) {
      fetchingSearchedMovie()
      navigate('/search/movie')
    }

  },[isClickedOnSearch])

  return (
    <div className="home-main-container">
      <div className="welcome-search-container">
        <div>
          <h1 className="welcome-heading">Welcome.</h1>
          <h1 className="welcome-description">
            Millions of movies, TV shows and people to discover.
          </h1>
          <h1 className="welcome-description">Explore now.</h1>
        </div>
        <div className="search-container">
          <input
            type="text"
            className="search-element"
            onChange={(event)=>handleSearch(event)}
            placeholder="Search for a movie, tv show, person....."
          />
          <button type="button" onClick={handleSearchButton} className="search-button">
            Search
          </button>
        </div>
      </div>
      <div className="home-middle-section">
        <Trending />
        <PopularMovies />
      </div>
    </div>
  );
};

export default Home;
