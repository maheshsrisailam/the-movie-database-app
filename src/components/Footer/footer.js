import './footer.css'

const Footer = () => {
    return (
        <div className='footer-main-container'>
            <div>
                <img src="https://pbs.twimg.com/profile_images/1243623122089041920/gVZIvphd_400x400.jpg" className='footer-logo' alt="logo" />
            </div>
            <div className='footer-cards'>
                <h1 className='footer-card-heading'>THE BASICS</h1>
                    <p className='footer-card-description'>About TMDB</p>
                    <p className='footer-card-description'>Contact Us</p>
                    <p className='footer-card-description'>Support Forums</p>
                    <p className='footer-card-description'>API</p>
                    <p className='footer-card-description'>System Status</p>
            </div>
            <div className='footer-cards'>
                <h1 className='footer-card-heading'>GET INVOLVED</h1>
                <p className='footer-card-description'>Contribution Bible</p>
                <p className='footer-card-description'>Add New Movie</p>
                <p className='footer-card-description'>Add New TV Show</p>
            </div>
            <div className='footer-cards'>
                <h1 className='footer-card-heading'>COMMUNITY</h1>
                <p className='footer-card-description'>Guidelines</p>
                <p className='footer-card-description'>Discussions</p>
                <p className='footer-card-description'>Leaderboard</p>
                <p className='footer-card-description'>Twitter</p>
            </div>
            <div className='footer-cards'>
                <h1 className='footer-card-heading'>LEGAL</h1>
                <p className='footer-card-description'>Terms of Use</p>
                <p className='footer-card-description'>API Terms of Use</p>
                <p className='footer-card-description'>Privacyy Policy</p>
            </div>
        </div>
    )
}

export default Footer;