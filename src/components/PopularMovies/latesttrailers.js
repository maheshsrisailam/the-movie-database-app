import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {actions} from '../../store/globalstate'
import './latesttrailers.css'

const latestTrailers = [
    {
        id: 1,
        streamingType: "Streaming"
    },
    {
        id: 2,
        streamingType: "On TV"
    },
    {
        id: 3,
        streamingType: "For Rent"
    },
    {
        id: 4,
        streamingType: "In Theatres"
    }
]

const formattedMovieData = (data) => ({
    adult: data.adult,
    backdropPath: data.backdrop_path,
    genreIds: data.genre_ids,
    id: data.id,
    originalLanguage: data.original_language,
    originalTitle: data.original_title,
    overview: data.overview,
    popularity: data.popularity,
    posterPath: data.poster_path,
    releaseDate: data.release_date,
    title: data.title,
    video: data.video,
    voteAverage: data.vote_average,
    voteCount: data.vote_count
})

const LatestTrailers = () => {
    const [streamingType,setStreamingType] = useState("Streaming")
    //const [popularMovies,setPopularMovies] = useState([])

    const handleStreaming = (streaming) => {
        setStreamingType(streaming)
    }
    const popularMovies = useSelector(state=>state.popularMovies)
    console.log(popularMovies)
    const navigate = useNavigate()

    const dispatch = useDispatch()

    useEffect(() => {
       const fetchingPopularMovies = async () => {
            const response = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=71025ca4064d6a5209a02f09d64b0c76&language=en-US&page=1`)
            if (response.ok) {
                const data = await response.json()
                const popularMoviesArray = data.results.map((movie) => formattedMovieData(movie))
                dispatch(actions.popularMovies(popularMoviesArray))
            }
        }

        fetchingPopularMovies()
        return () => {
            dispatch(actions.popularMovies([]))
        }
    },[])

    const handlePopularMovie = (movie) => {
        dispatch(actions.movieDetails(movie))
        //dispatch({type:"MOVIEDETAILS",payload: movieDetails})
        navigate('/moviedetails')
    }

    return (
        <div className='latest-trailers-main-container'>
            <div className='latest-trailers-streaming'>
                <h1 className='latest-trailers'>Popular Movies</h1>
                <div className='streaming-on-tv-for-rent'>
                    {
                        latestTrailers.map((streaming) => {
                            const activeLatestTrailersButton = streamingType === streaming.streamingType ? "streaming-active": "streaming"
                            return (<button type='button' key={streaming.id} onClick={()=>handleStreaming(streaming.streamingType)} className={activeLatestTrailersButton}>{streaming.streamingType}</button>)} )
                    }
                </div>
            </div>
            <div className='popular-movies-section'>
            {
                popularMovies.map((movie)=>{
                    return (
                        <div className='popular-movie-card' key={movie.id} onClick={()=>handlePopularMovie(movie)}>
                            <img src={`https://image.tmdb.org/t/p/w500/${movie.backdropPath}`} className="popular-movie-image" alt="movie" />
                            <h2 className='popular-movie-title'>{movie.title}</h2>
                            <p className='popular-movie-release-date'>{movie.releaseDate}</p>
                        </div>
                    )
                })
            }
            </div>
        </div>
    )
}

export default LatestTrailers;