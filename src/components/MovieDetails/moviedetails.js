import { useSelector } from "react-redux";
import './moviedetails.css'

const MovieDetails = () => {
    const movieDetails = useSelector((state) => state.movieDetails)

    return (
        <div className="movie-details-main-container">
            <div className="movie-details-card">
            <img src={`https://image.tmdb.org/t/p/w500/${movieDetails.posterPath}`} className="movie-details-image" alt="movie" />
            <div className="movie-details-description-section">
                <h3 className="movie-details-title">Movie Title : <span>{movieDetails.title}</span></h3>
                <p className="movie-details-description">Language : <span>{movieDetails.originalLanguage}</span></p>
                <p className="movie-details-description">Overview : <span className="overview">{movieDetails.overview}</span></p>
                <p className="movie-details-description">Release Date : <span>{movieDetails.releaseDate}</span></p>
                <p className="movie-details-description">Popularity : <span>{movieDetails.popularity}</span></p>
                <p className="movie-details-description">Vote Average : <span>{movieDetails.voteAverage}</span></p>
                <p className="movie-details-description">Vote Count : <span>{movieDetails.voteCount}</span></p>
            </div>
            </div>
        </div>
    )

}

export default MovieDetails;