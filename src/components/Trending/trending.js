import { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import {actions} from '../../store/globalstate'
import './trending.css'

const trendingTypeArray = [
    {
        id: 1,
        trendingIn:"Today",
        trendingInId:"day"

    },
    {
        id: 2,
        trendingIn: "This Week",
        trendingInId:"week"
    }
]


const formattedMovieData = (data) => ({
    adult: data.adult,
    backdropPath: data.backdrop_path,
    releaseDate: data.first_air_date,
    genreIds: data.genre_ids,
    id: data.id,
    mediaType: data.media_type,
    name: data.name,
    originCountry: data.origin_country,
    originalLanguage: data.original_language,
    originalName: data.original_name,
    overview: data.overview,
    popularity: data.popularity,
    posterPath: data.poster_path,
    voteAverage: data.vote_average,
    voteCount: data.vote_count
})

const Trending = () => {
    const [timeWindow,setTimeWindow] = useState("day")

    const dispatch = useDispatch()
    const trendingMoviesData = useSelector(state=>state.trendingMoviesData)
    console.log(trendingMoviesData)
    const handleTrending = (trendingIn) => {
        setTimeWindow(trendingIn)
    }

    useEffect(() => {
        const fetchingTheTrendingVideos = async () => {
            const response = await fetch(`https://api.themoviedb.org/3/trending/all/${timeWindow}?api_key=71025ca4064d6a5209a02f09d64b0c76`)
            if (response.ok) {
                const data = await response.json()
                const trendingMovies = data.results.map((movieDetails)=>formattedMovieData(movieDetails))
                // dispatch({type:"TRENDINGMOVIES",payload: trendingMovies})
                dispatch(actions.trendingMovies(trendingMovies))
            }
        }

        fetchingTheTrendingVideos()

        return () => {
            // dispatch({type: "TRENDINGMOVIES", payload:[]})
            dispatch(actions.trendingMovies([]))
        }
    },[timeWindow])

    const navigate= useNavigate()

    const handleTrendingMovie = (movie) => {
        //dispatch({type:"MOVIEDETAILS",payload:movieDetails})
        dispatch(actions.movieDetails(movie))
        navigate('/moviedetails')
    }

    return (
        <div className='trending-main-container'>
            <div className='trending-today-this-week'>
                <h1 className='trending'>Trending</h1>
                <div className='today-this-week-container'>
                    {
                        trendingTypeArray.map((trending)=> {
                            const trendingButtonStyle = timeWindow === trending.trendingInId ? "active" : "today"
                            return(<button type='button' key={trending.id} onClick={()=>handleTrending(trending.trendingInId)} className={trendingButtonStyle}>{trending.trendingIn}</button>)})
                    }
                </div>
            </div>
            <div className='trending-movies-section'>
            {
                trendingMoviesData.map((movie)=>{
                    return (
                        <div className='trending-movie-card' onClick={()=>handleTrendingMovie(movie)} key={movie.id}>
                            <img src={`https://image.tmdb.org/t/p/w500/${movie.backdropPath}`} className="trending-movie-image" alt="movie" />
                            <h2 className='movie-title'>{movie.name}</h2>
                            <p className='movie-release-date'>{movie.releaseDate}</p>
                        </div>
                    )
                })
            }
            </div>
        </div>
    )
}

export default Trending;