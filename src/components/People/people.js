import { useEffect } from "react";

const People = () => {
    useEffect(()=>{
        const fetchingPopularPeople = async () => {
            const response = await fetch(`https://api.themoviedb.org/3/person/popular?api_key=71025ca4064d6a5209a02f09d64b0c76&language=en-US&page=1`)
            if (response.ok) {
                const data = await response.json()
                console.log(data.results)
            }
        }
        fetchingPopularPeople()
    },[])

    return ( 
        <div>
            People
        </div>
     );
}
 
export default People;