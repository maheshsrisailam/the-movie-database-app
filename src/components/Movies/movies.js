import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { actions } from "../../store/globalstate";
import './movies.css'

const formattedMovieData = (data) => ({
    adult: data.adult,
    backdropPath: data.backdrop_path,
    genreIds: data.genre_ids,
    id: data.id,
    originalLanguage: data.original_language,
    originalTitle: data.original_title,
    overview: data.overview,
    popularity: data.popularity,
    posterPath: data.poster_path,
    releaseDate: data.release_date,
    title: data.title,
    video: data.video,
    voteAverage: data.vote_average,
    voteCount: data.vote_count
})

const Movies = () => {

    const dispatch = useDispatch()
    const allMovies = useSelector((state)=>state.allMovies)
    const navigate = useNavigate()

    useEffect(() => {
        const fetchingMovies = async () => {
            const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=71025ca4064d6a5209a02f09d64b0c76&language=en-US&page=1`)
            if (response.ok) {
                const data = await response.json()
                //console.log(data.results)
                const movies = data.results.map((movie) => formattedMovieData(movie))
                //dispatch({type: "ALLMOVIES",payload: movies})
                dispatch(actions.allMovies(movies))
            }
        }
        fetchingMovies()

        return () => {
            dispatch(actions.allMovies([]))
        }
    },[])

    const handleMovie = (movie) => {
        //dispatch({type:"MOVIEDETAILS", payload:movie})
        dispatch(actions.movieDetails(movie))
        navigate('/moviedetails')
    } 

    return ( 
        <div className="movies-main-container">
            <div className="movies-inner-container">
                {
                    allMovies.map((movie)=>{
                        return (
                            <div className="movie-card" key={movie.id} onClick={()=>handleMovie(movie)}>
                                <img src={`https://image.tmdb.org/t/p/w500/${movie.backdropPath}`} className="movie-card-image" alt="image" />
                                <p className="movie-title">{movie.title}</p>
                                <p className="movie-release-date">{movie.releaseDate}</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>
     );
}
 
export default Movies;