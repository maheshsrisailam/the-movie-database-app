import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { actions } from "../../store/globalstate";
import './searchresults.css'

const SearchResults = () => {
    const movies = useSelector(state=>state.movieSearchResults)
    const dispatch = useDispatch()
    const searchInput = useSelector(state=>state.searchInput)
    const navigate = useNavigate()
    const handleSearchedMovie = (movie) => {
        // dispatch({type: "MOVIEDETAILS",payload: movie})
        dispatch(actions.movieDetails(movie))
        navigate("/moviedetails")
    }
    return ( 
        <div className="search-main-container">
            {movies.length !== 0 ? 
            <div className="search-movies-section">
            {
                movies.map((movie) => {
                    const {id,title,overview,releaseDate,backdropPath} = movie
                    return (
                        <div onClick={()=>handleSearchedMovie(movie)} key={id} className="search-movie-card">
                            <img src={`https://image.tmdb.org/t/p/w500/${backdropPath}`} className="search-movie-image" alt="movie" />
                            <div className="search-movie-description">
                                <h3 className="search-movie-title">{title}</h3>
                                <p className="searched-movie-release-date">{releaseDate}</p>
                                <p className="searched-movie-overview">{overview}</p>
                            </div>
                        </div>
                    )
                })
            }
        </div>
            :
            <div className="no-movies-section">
                <h2>{`Sorry, There are no movies that matched your query ${searchInput}.`} </h2>
            </div>
            }
        </div>
     );
}
 export default SearchResults;