import { FaPlus } from "react-icons/fa";
import { MdNotifications } from "react-icons/md";
import { BiSearch } from "react-icons/bi";
import { RxCross2 } from "react-icons/rx";
import "./header.css";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="header-main-container">
      <div className="features-container">
        <div className="website-logo">
          <Link to="/" className="link-style">
            <img
              src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.png"
              className="website-logo"
              alt="tmdb"
            />
          </Link>
        </div>

        <Link to="/movie" className="link-style">
          <h1 className="header-features">Movies</h1>
        </Link>
        <Link className="link-style">
          <h1 className="header-features">TV Shows</h1>
        </Link>
        <Link className="link-style">
          <h1 className="header-features">People</h1>
        </Link>
        <h1 className="header-features">More</h1>
      </div>
      <div className="profile-search-container">
        <FaPlus className="icons-style" />
        <div className="language-container">
          <h1 className="language">EN</h1>
        </div>
        <MdNotifications className="icons-style" />
        <div className="profile-container">
          <h3 className="profile-name">M</h3>
        </div>
        {/* {isClickedOnSearch ? <button type="button" onClick={handleSearch} className='header-search-button'><RxCross2 className="icons-style" /></button>: <button type="button" onClick={handleSearch} className='header-search-button'><BiSearch className="icons-style" /></button>} */}
      </div>
    </div>
  );
};

export default Header;
