import { createSlice,configureStore } from "@reduxjs/toolkit";
/* import {createStore} from 'redux'

const initialState = {
    isClickedOnSearch: false,
    searchInput:"",
    allMovies:[],
    movieSearchResults:[],
    trendingMoviesData:[],
    popularMovies: [],
    movieDetails: {}
}

const reducer = (state=initialState,action) => {
    switch(action.type) {
        case "SEARCH":
            return {...state,isClickedOnSearch:!state.isClickedOnSearch}
        case "SEARCH_INPUT":
            return {...state, searchInput: action.payload}
        case "SEARCH_RESULTS":
            return {...state, movieSearchResults: action.payload }
        case "ALLMOVIES":
            return {...state, allMovies: action.payload}
        case "TRENDINGMOVIES":
            return {...state,trendingMoviesData:action.payload}
        case "MOVIEDETAILS":
            return {...state,movieDetails:action.payload}
        default:
            return state
    }
}

const store = createStore(reducer)
export default store; */

const tmdbMoviesSlice = createSlice({
    name : "tmDbMovies",
    initialState : {
        isClickedOnSearch: false,
        searchInput:"",
        allMovies:[],
        movieSearchResults:[],
        trendingMoviesData:[],
        popularMovies: [],
        movieDetails: {}
    },
    reducers: {
        search(state,action) {
            return {...state,isClickedOnSearch:!state.isClickedOnSearch}
        },
        searchInput(state,action) {
            return {...state, searchInput: action.payload}
        },
        searchResults(state,action) {
            return {...state, movieSearchResults: action.payload }
        },
        allMovies(state,action) {
            return {...state, allMovies: action.payload}
        },
        trendingMovies(state,action) {
            return {...state,trendingMoviesData:action.payload}
        },
        popularMovies(state,action) {
            return {...state,popularMovies:action.payload}
        },
        movieDetails(state,action) {
            return {...state,movieDetails:action.payload}
        }
    }
});

export const actions = tmdbMoviesSlice.actions

const store = configureStore({
    reducer: tmdbMoviesSlice.reducer
})

export default store;