import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header/header";
import Footer from "./components/Footer/footer";
import Home from "./components/Home/home";
import Movies from './components/Movies/movies'
import TvShows from "./components/TvShows/tvshows";
import People from "./components/People/people";
import MovieDetails from './components/MovieDetails/moviedetails'
import {BiSearch} from 'react-icons/bi'
import { useSelector } from "react-redux";
import SearchResults from "./components/SearchResults/searchresults";

const App = () => {
  const isClickedOnSearch = useSelector(state=>state.isClickedOnSearch)
  const searchElementStyle = isClickedOnSearch ? "search-active": "search-inactive"
  return (
      <BrowserRouter>
        <Header />
        {/* <div className={`app-search-container ${searchElementStyle}`}>
          <BiSearch className="app-search-icon" />
          <input type="search" className="app-search-element" placeholder="Search for a movie, tv show, person....." />
        </div> */}
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/movie" element={<Movies />} />
          <Route path="/tvshow" element={<TvShows />} />
          <Route path="/people" element={<People />} />
          <Route path="/movieDetails" element={<MovieDetails />} />
          <Route path="/search/movie" element={<SearchResults />} />
        </Routes>
        <Footer />
      </BrowserRouter>
  );
};

export default App;
